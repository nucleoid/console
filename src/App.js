import APIManagement from "./pages/ide/APIManagement";
import Dev from "./pages/Dev";
import React from "react";
import Services from "./pages/console/Services";
import theme from "./theme";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import { Redirect, Route, BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Route
          exact
          path="/"
          render={() => <Redirect to="/ide/api-management" />}
        />
        <Route path={["/dev"]} component={Dev} />
        <Route path={"/console/services"} component={Services} />
        <Route path={"/ide/api-management"} component={APIManagement} />
      </Router>
    </ThemeProvider>
  );
}

export default App;
