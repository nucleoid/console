import Drawer from "@material-ui/core/Drawer";
import Logo from "./Logo";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";

const width = 300;

export const useStyles = makeStyles(() => ({
  root: {
    width,
    flexShrink: 0,
  },
  drawer: {
    width,
  },
  item: {
    background: "#353e48",
    color: "rgba(255, 255, 255, 0.7)",
    "&:hover,&:focus": {
      color: "rgba(255, 255, 255, 1)",
    },
  },
  icon: {
    color: "rgba(255, 255, 255, 0.7)",
  },
}));

function Menu(props) {
  const classes = useStyles();

  return (
    <nav className={classes.root}>
      <Drawer
        variant="permanent"
        classes={{
          paper: classes.drawer,
        }}
      >
        <List>
          <ListItem>
            <Logo title={props.title} />
          </ListItem>
          <br />
          {props.list.map(({ title, icon }) => (
            <React.Fragment key={title}>
              <ListItem className={classes.item} button>
                <ListItemIcon className={classes.icon}>{icon}</ListItemIcon>
                <ListItemText primary={title} />
              </ListItem>
            </React.Fragment>
          ))}
        </List>
      </Drawer>
    </nav>
  );
}

export default Menu;
