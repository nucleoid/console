import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React from "react";
import { TreeItem, TreeView } from "@material-ui/lab";

function APITree({ apis }) {
  const graph = {};

  for (let i = 0; i < apis.length; i++) {
    const api = apis[i];
    const parts = api.path.substring(1).split("/");
    const label = "/" + parts.pop();
    const parent = "/" + parts.join("/");

    const node = { label, path: api.path, resources: [], methods: api.methods };

    if (graph[parent]) graph[parent].resources.push(node);

    graph[api.path] = node;
  }

  return (
    <TreeView
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      expanded={apis.map((api) => api.path)}
    >
      {compile([graph["/"]])}
    </TreeView>
  );
}

const compile = (apis) =>
  apis.map((api) => {
    let children = undefined;

    if (api.resources && api.resources.length > 0) {
      children = compile(api.resources);
    }

    children = api.methods
      .map((method) => (
        <TreeItem
          key={`${api.path}-${method}`}
          nodeId={`${api.path}-${method}`}
          label={method}
        />
      ))
      .concat(children);

    return (
      <TreeItem
        key={api.path}
        nodeId={api.path}
        label={api.label}
        children={children}
      />
    );
  });

export default APITree;
