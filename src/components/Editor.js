import AceEditor from "react-ace";
import React from "react";

function Editor() {
  return <AceEditor style={{ width: "100%" }} />;
}

export default Editor;
