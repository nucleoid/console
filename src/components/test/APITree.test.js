import APITree from "../APITree";
import Adapter from "enzyme-adapter-react-16";
import React from "react";
import { TreeItem } from "@material-ui/lab";
import Enzyme, { shallow } from "enzyme";

Enzyme.configure({ adapter: new Adapter() });

test("List nested APIs", () => {
  const apis = [
    {
      path: "/",
      methods: [],
    },
    {
      path: "/questions",
      methods: [],
    },
  ];

  const wrapper = shallow(<APITree apis={apis} />);
  const root = wrapper.find(TreeItem).first();
  const child = root.children().first();

  expect(root.prop("label")).toEqual("/");
  expect(child.prop("label")).toEqual("/questions");
});

test("List APIs with methods", () => {
  const apis = [
    {
      path: "/",
      methods: [],
    },
    {
      path: "/questions",
      methods: ["GET", "POST"],
    },
  ];

  const wrapper = shallow(<APITree apis={apis} />);
  const root = wrapper.find(TreeItem).first();
  const child = root.children().first();
  const methods = child.children();

  expect(methods.first().prop("label")).toEqual("GET");
  expect(methods.at(1).prop("label")).toEqual("POST");
});
