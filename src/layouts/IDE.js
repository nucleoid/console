import FolderIcon from "@material-ui/icons/Folder";
import Menu from "../components/Menu";
import React from "react";
import SendIcon from "@material-ui/icons/Send";
import SettingsEthernetIcon from "@material-ui/icons/SettingsEthernet";
import StorageIcon from "@material-ui/icons/Storage";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  panel: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
  },
  main: {
    flex: 1,
    padding: theme.spacing(1, 1),
  },
}));

const list = [
  { title: "API Management", icon: <SendIcon /> },
  { title: "Components", icon: <FolderIcon /> },
  { title: "Query", icon: <StorageIcon /> },
  { title: "Stages", icon: <SettingsEthernetIcon /> },
];

function IDE(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Menu list={list} title="IDE" />
      <div className={classes.panel}>
        <main className={classes.main}>{props.children}</main>
      </div>
    </div>
  );
}

export default IDE;
