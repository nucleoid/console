import { createMuiTheme } from "@material-ui/core";

let theme = createMuiTheme({
  palette: {
    background: {
      default: "#eaeff1",
    },
  },
});

theme = {
  ...theme,
  overrides: {
    MuiDrawer: {
      paper: {
        backgroundColor: "#323a40",
      },
    },
  },
};

export default theme;
