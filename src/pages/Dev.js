import APITree from "../components/APITree";
import Logo from "../components/Logo";
import React from "react";
import { Grid, Paper } from "@material-ui/core";

const apis = [
  {
    path: "/",
    methods: ["GET"],
  },
  {
    path: "/devices",
    methods: ["GET", "POST"],
  },
  {
    path: "/questions",
    methods: ["GET", "POST", "DELETE", "PUT"],
  },
  {
    path: "/questions/reviews",
    methods: ["POST"],
  },
];

function Dev() {
  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      style={{ minHeight: "100vh" }}
    >
      <Logo title="Dev" />
      <br />
      <Paper style={{ height: 300, width: 300 }}>
        <APITree apis={apis} />
      </Paper>
    </Grid>
  );
}

export default Dev;
