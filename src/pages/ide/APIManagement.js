import APITree from "../../components/APITree";
import Editor from "../../components/Editor";
import IDE from "../../layouts/IDE";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Paper } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paper: {
    minHeight: window.innerHeight - theme.spacing() * 2 - 1,
  },
}));

const apis = [
  {
    path: "/",
    methods: ["GET"],
  },
  {
    path: "/devices",
    methods: ["GET", "POST"],
  },
  {
    path: "/questions",
    methods: ["GET", "POST", "DELETE", "PUT"],
  },
  {
    path: "/questions/reviews",
    methods: ["POST"],
  },
];

function APIManagement() {
  const classes = useStyles();

  return (
    <IDE>
      <Grid container spacing={1}>
        <Grid item xs={3}>
          <Paper className={classes.paper}>
            <APITree apis={apis} />
          </Paper>
        </Grid>
        <Grid item xs={9}>
          <Paper className={classes.paper}>
            <Editor />
          </Paper>
        </Grid>
      </Grid>
    </IDE>
  );
}

export default APIManagement;
